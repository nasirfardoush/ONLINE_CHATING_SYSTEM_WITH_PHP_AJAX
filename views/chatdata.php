<?php
	include_once('../vendor/autoload.php');
	$obj = new App\Chat;
	$data = $obj->index();


   foreach ($data as $value) { ?>
   	
    <li class="left clearfix"><span class="chat-img pull-left">
        <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
    </span>
        <div class="chat-body clearfix">
            <div class="header">
                <strong class="primary-font"><?php echo $value['name']; ?></strong> <small class="pull-right text-muted">
                    <span class="glyphicon glyphicon-time"></span><?php echo $value['date']; ?></small>
            </div>
            <p>
               <?php echo $value['message']; ?>
            </p>
        </div>
    </li>
    <?php  } ?>